import { Injectable } from '@angular/core';

import { State } from './home.model';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

 private states: State[] = [
    {
      id: 's1',
      name: 'Sachsen',
      imageUrl: 'https://www.fahnenwelt.com/media/image/e9/db/27/50080424.jpg',
      stateLink:'https://www.coronavirus.sachsen.de/download/sms-Aktuelle-Regelungen-2021-04-23.pdf'
    },

    {
      id: 's2',
      name: 'Schleswig Holstein',
      imageUrl: 'https://www.fahnenwelt.com/media/image/82/2d/28/50080432_600x600.jpg',
      stateLink:'https://www.schleswig-holstein.de/DE/Schwerpunkte/Coronavirus/_documents/teaser_erlasse.html'
    },

    {
      id: 's3',
      name: 'Mecklenburg Vorpommern',
      imageUrl: 'https://www.fahnenwelt.com/media/image/97/92/ab/50080430_600x600.jpg',
      stateLink: 'https://www.regierung-mv.de/corona/Verordnungen-und-Dokumente/'

    },

    {
      id: 's4',
      name: 'Niedersachsen',
      imageUrl: 'https://www.fahnenwelt.com/media/image/84/8a/68/50080429_600x600.jpg',
      stateLink: 'https://www.niedersachsen.de/Coronavirus/vorschriften-der-landesregierung-185856.html'

    },

    {
      id: 's5',
      name: 'Hamburg',
      imageUrl: 'https://www.fahnenwelt.com/media/image/6e/cb/e7/50080427.jpg',
      stateLink: 'https://www.hamburg.de/allgemeinverfuegungen/'

    },
    {
      id: 's6',
      name: 'Bremen',
      imageUrl: 'https://www.fahnenwelt.com/media/image/9a/8f/8f/50080426_600x600.jpg',
      stateLink: 'https://www.bremen.de/corona#inzidenz-aktuell'
    },
    {
      id: 's7',
      name: 'Nordrhein Wesfalen',
      imageUrl: 'https://www.fahnenwelt.com/media/image/08/06/ca/50080433_600x600.jpg',
      stateLink: 'https://www.land.nrw/corona'
    },
    {
      id: 's8',
      name: 'Sachsen Anhalt',
      imageUrl: 'https://www.fahnenwelt.com/media/image/46/de/e2/50080425_600x600.jpg',
      stateLink: 'https://ms.sachsen-anhalt.de/themen/gesundheit/aktuell/coronavirus/verordnungen-erlasse-und-empfehlungen/'
    },
    {
      id: 's9',
      name: 'Brandenburg',
      imageUrl: 'https://www.fahnenwelt.com/media/image/93/02/85/50080419_600x600.jpg',
      stateLink: 'https://kkm.brandenburg.de/kkm/de/'
    },
    {
      id: 's10',
      name: 'Berlin',
      imageUrl: 'https://www.fahnenwelt.com/media/image/e0/96/ba/50080434_600x600.jpg',
      stateLink: 'https://www.berlin.de/corona/massnahmen/verordnung/'

    },

    {
      id: 's11',
      name: 'Thüringen',
      imageUrl: 'https://www.fahnenwelt.com/media/image/e2/21/52/50080423_600x600.jpg',
      stateLink:'https://www.tmasgff.de/covid-19/rechtsgrundlage'
    },

    {
      id: 's12',
      name: 'Hessen',
      imageUrl: 'https://www.fahnenwelt.com/media/image/4a/09/c6/50080422_600x600.jpg',
      stateLink:'https://www.hessen.de/sites/default/files/media/corona_regeln_hessen_aktuell-2904-final.pdf'
    },

    {
      id: 's13',
      name: 'Rheinland Pfalz',
      imageUrl: 'https://www.fahnenwelt.com/media/image/e8/33/89/50080435_600x600.jpg',
      stateLink:'https://corona.rlp.de/de/aktuelles/corona-regeln-im-ueberblick/'
    },

    {
      id: 's14',
      name: 'Saarland',
      imageUrl: 'https://www.fahnenwelt.com/media/image/18/54/9c/500804231_600x600.jpg',
      stateLink:'https://www.saarland.de/DE/portale/corona/service/rechtsverordnung-massnahmen/rechtsverordnung-massnahmen_node.html'
    },

    {
      id: 's15',
      name: 'Baden Württemberg',
      imageUrl: 'https://www.fahnenwelt.com/media/image/a0/41/09/50080436_600x600.jpg',
      stateLink:'https://www.baden-wuerttemberg.de'
    },

    {
      id: 's16',
      name: 'Bayern',
      imageUrl: 'https://www.fahnenwelt.com/media/image/a3/aa/2a/50080428_600x600.jpg',
      stateLink: 'https://www.muenchen.de/aktuell/2021/2021-04/corona-massnahmen-2804-bayern.html'
    }

  ];


  constructor() { }

  getAllStates(){
  return [...this.states];
}

getState(stateId: string){
    this.states.find(state => state.id === stateId);
    return this.states;
}
}
