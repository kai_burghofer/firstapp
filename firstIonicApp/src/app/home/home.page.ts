import { Component, OnInit } from '@angular/core';

import { State } from './home.model';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  states: State[];

  constructor(private homeService: HomeService) {}

  ngOnInit(){
    this.states = this.homeService.getAllStates();
}

}
