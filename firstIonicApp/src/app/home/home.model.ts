export interface State {
  id: string;
  name: string;
  imageUrl: string;
  stateLink: string;
}
