import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { State } from '../home.model';
import { HomeService } from '../home.service';

@Component({
  selector: 'app-state-detail',
  templateUrl: './state-detail.page.html',
  styleUrls: ['./state-detail.page.scss'],
})
export class StateDetailPage implements OnInit {
loadedstate: State [];

  constructor(private activatedRoute: ActivatedRoute, private homeService: HomeService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('stateId')) {
        return;
      }
      const stateId = paramMap.get('stateId');
      this.loadedstate = this.homeService.getState(stateId);

    }
      );
  }

}
