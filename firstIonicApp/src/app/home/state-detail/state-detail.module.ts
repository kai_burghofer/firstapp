import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StateDetailPageRoutingModule } from './state-detail-routing.module';

import { StateDetailPage } from './state-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StateDetailPageRoutingModule
  ],
  declarations: [StateDetailPage]
})
export class StateDetailPageModule {}
